﻿using Puissance4.Models;
using System.Text.RegularExpressions;
using Group = Puissance4.Models.Group;

namespace Puissance4.Infrastructure
{
    public class GroupManager
    {
        private List<Group> groupList;

        public GroupManager()
        {
            groupList = new List<Group>();
        }

        public List<Group> GetGroups()
        {
            return groupList;
        }

        public void AddPlayerToGroup(Player p, string groupName)
        {
            Group group = groupList.FirstOrDefault(g => g.GroupName == groupName);
            if (group == null)
            {
                group = new Group(groupName);
                groupList.Add(group);
            }
            
            group.PlayerList.Add(p);
        }
    }
}
