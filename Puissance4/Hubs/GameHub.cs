﻿using Microsoft.AspNetCore.SignalR;
using Puissance4.Models;

namespace Puissance4.Hubs
{
    public class GameHub : Hub
    {
        public async Task JoinGame (string groupName, string playerName)
        {
            await Groups.AddToGroupAsync (Context.ConnectionId, playerName);
            await SendToGroup(playerName + " à rejoint la partie", groupName);
        }

        public async Task SendToGroup(string message, string groupName)
        {
            await Clients.Group(groupName).SendAsync("to" + groupName, message);
        }

        public async Task LeaveGame(string groupName)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
        }
    }
}
