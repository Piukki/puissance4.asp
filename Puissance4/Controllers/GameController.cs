﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Puissance4.Infrastructure;

namespace Puissance4.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameController : ControllerBase
    {
        private GroupManager _groupManager;

        public GameController(GroupManager groupManager)
        {
            _groupManager = groupManager;
        }

        [HttpGet("group")]
        public IActionResult GetGroups()
        {
            return Ok(_groupManager.GetGroups());
        }

    }
}
