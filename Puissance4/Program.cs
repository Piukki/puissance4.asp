using Puissance4.Controllers;
using Puissance4.Hubs;
using Puissance4.Infrastructure;

namespace Puissance4
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddSingleton<GroupManager>();

            builder.Services.AddSignalR();

            builder.Services.AddCors(o =>
            {
                o.AddPolicy("SignalRCorsPolicy",
                    o => o.WithOrigins("http://localhost:4200")
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials());
            });

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.UseCors("SignalRCorsPolicy");

            app.MapControllers();
            app.MapHub<GameHub>("gamehub");

            app.Run();
        }
    }
}