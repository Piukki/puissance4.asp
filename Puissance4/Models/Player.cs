﻿namespace Puissance4.Models
{
    public class Player
    {
        public string Username { get; set; }
        public string ConnectionId { get; set; }
    }
}
