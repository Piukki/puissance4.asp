﻿namespace Puissance4.Models
{
    public class Group
    {
        public string GroupName { get; set; }
        public List<Player> PlayerList { get; set; }

        public Group(string groupName)
        {
            GroupName = groupName;
            PlayerList = new List<Player>();
        }
    }
}
